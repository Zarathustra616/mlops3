import click
import nltk
import re
import pandas as pd
import sklearn
import pymorphy2
from dostoevsky.tokenization import RegexTokenizer
from dostoevsky.models import FastTextSocialNetworkModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, recall_score, precision_score, f1_score
from sklearn.model_selection import train_test_split


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def train_model(input_filepath, output_filepath):
    df = pd.read_csv(input_filepath, delimiter=";", on_bad_lines='skip', encoding="UTF-8", index_col=0)
    DF = pd.DataFrame(columns=['text'], index=range(len(df)))  # Создание дополнительного датафрейма для хранения текста

    for i in range(len(df)):
        DF.iloc[i]['text'] = str(df.iloc[i]['ttext'])
        DF.iloc[i]['text'] = DF.iloc[i]['text'].lower()  # Преобразование текста к строковому типу в нижнем регистре

    for i in range(len(DF)):
        DF.iloc[i]['text'] = re.sub(r'[^А-Яа-я\s]', ' ', DF.iloc[i]['text'],
                                    flags=re.UNICODE)  # Удаление всех символов кроме кириллицы
        DF.iloc[i]['text'] = ' '.join(DF.iloc[i]['text'].split())
        DF.iloc[i]['text'] = DF.iloc[i]['text'].strip()

    morph = pymorphy2.MorphAnalyzer()

    for i in range(len(DF)):
        word_list = re.split('\s+', DF.iloc[i]['text'])
        from nltk.corpus import stopwords
        stopWords = stopwords.words("russian")

        def lineWithoutStopWords(word_list):
            return [word for word in word_list if word not in stopWords]

        withoutStopWords = lineWithoutStopWords(word_list)  # Удаление стоп слов
        lemmatized_output = ' '.join([morph.parse(w)[0].normal_form for w in withoutStopWords])
        DF.iloc[i]['text'] = lemmatized_output

    tokenizer = RegexTokenizer()

    model = FastTextSocialNetworkModel(tokenizer=tokenizer)
    messages = DF['text']

    results = model.predict(messages, k=5)

    sent = []
    for message, sentiment in zip(messages, results):
        sent.append(sentiment)

    ndf = pd.DataFrame(sent)

    ndf['type'] = df['ttype'].values

    ndf.drop(ndf.columns.difference(['positive', 'negative', 'type']), 1, inplace=True)

    Y = ndf['type']
    X = ndf.drop('type', axis=1)
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.3,
                                                        shuffle=True)  # Разделение на обучающую и тестовую выборки

    # сравниваем результаты работы библиотеки и изначальной разметки
    LR = LogisticRegression()
    LR.fit(X_train, Y_train)
    Y_LR = LR.predict(X_test)


    output_write = open(output_filepath, 'w')
    output_write.write('Accuracy: ' + str(round(accuracy_score(Y_test, Y_LR), 3)))
    output_write.write('\n Precision: ' + str(round(precision_score(Y_test, Y_LR), 3)))
    output_write.write('\n Recall: ' + str(round(recall_score(Y_test, Y_LR), 3)))
    output_write.write('\n F1_score: ' + str(round(f1_score(Y_test, Y_LR), 3)))
    output_write.close()


if __name__ == '__main__':
    train_model()
