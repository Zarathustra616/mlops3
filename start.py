import src

RAW_DATA_PATH = 'data/raw/tweets_good.csv'
MAKE_DATASET_PATH = 'data/interim/make_dataset.csv'
START_FEATURE_PATH = 'data/processed/start_feature.txt'
PREDICT_MODEL_PATH = 'data/processed/predict_model.txt'

if __name__ == '__main__':
    src.make_dataset(RAW_DATA_PATH, MAKE_DATASET_PATH)
    src.stat_feature(MAKE_DATASET_PATH, START_FEATURE_PATH)
    src.train_model(MAKE_DATASET_PATH, PREDICT_MODEL_PATH)