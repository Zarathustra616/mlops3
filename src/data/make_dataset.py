# -*- coding: utf-8 -*-
import click
import pandas as pd



@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def make_dataset(input_filepath, output_filepath):
    df = pd.read_csv(input_filepath, delimiter=";", on_bad_lines='skip', encoding="UTF-8", index_col=0)
    df.drop(['index'], axis=1)

    from datetime import datetime
    df['tdate'] = [datetime.fromtimestamp(x) for x in df['tdate']]
    df[['trtw', 'tfav']] = df[['trtw', 'tfav']].astype('int')

    df.to_csv(output_filepath, sep =';')


if __name__ == '__main__':
    make_dataset()
