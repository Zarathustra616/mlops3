import click
import pandas as pd


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def stat_feature(input_filepath, output_filepath):
    df = pd.read_csv(input_filepath, delimiter=";", on_bad_lines='skip', encoding="UTF-8", index_col=0)

    df = df.sort_values(by='tstcount', ascending=False)
    dfi = df['tmane'].unique()

    negative = df.loc[df['ttype'] == 0]
    negative['tmane'].value_counts()
    positive = df.loc[df['ttype'] == 1]
    positive['tmane'].value_counts()
    df.sort_values(by='tfol', ascending=False)

    VBnegative = negative.loc[df['tmane'] == 'muz_tv']  # Поиск наиболее популярного пользователя среди негативных твитов
    VBnegative['tmane'].value_counts()

    VBpositive = positive.loc[df['tmane'] == 'muz_tv']  # Поиск наиболее популярного пользователя среди позитивных твитов
    VBpositive['tmane'].value_counts()

    negative['tfav'].value_counts()
    positive['tfav'].value_counts()

    df.sort_values(by='tfrien', ascending=False)  # Пользователи с наибольшим кольчеством друзей

    FRnegative = negative.loc[df['tmane'] == 'olegintwitt']  # Поиск пользователя с наибольшим кол-вом друзей среди негативных твитов
    FRnegative['tmane'].value_counts()

    FRpositive = positive.loc[df['tmane'] == 'olegintwitt']  # Поиск пользователя с наибольшим кол-вом друзей среди позитивныых твитов
    FRpositive['tmane'].value_counts()

if __name__ == '__main__':
    stat_feature()